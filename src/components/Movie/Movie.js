import React from 'react';

const Movie = ({data}) => (
<div>
    <div>
        <h2>{data.Title}</h2>
        <p>
            <span>Date de sortie au cinéma : </span>
            {data.Released}
        </p>
        <p>
            <span>Date de sortie DVD : </span>
            {data.DVD}
        </p>
        <p>
            <span>Durée du film : </span>
            {data.Runtime}
        </p>
        <p>
            <span>Note de IMBD : </span>
            {data.imdbRating} /10
        </p>
        <p>
            <span>Genre : </span>
            {data.Genre}
        </p>
        <p>
            <span>Réalisateur : </span>
            {data.Director}
        </p>
        <p>
            <span>Acteurs : </span>
            {data.Actors}
        </p>
        <p>
            <span>Déconseillé aux moins de : </span>
            {data.Rated}
        </p>
        <p>
            <span>Résumé : </span>
            {data.Plot}
        </p>
        <p>
            <span>Production : </span>
            {data.Production}
        </p>
        <p>
            <span>Récompenses : </span>
            {data.Awards}
        </p>
        <p>
            <span>Recettes du film : </span>
            {data.BoxOffice}
        </p>
    </div>
    <img src={`${data.Poster}`} alt=""/>
</div>
);

export default Movie;