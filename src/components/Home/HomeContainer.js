import React, { Component, createRef } from 'react';
import Home from './Home';
import {api} from '../../constants/config';

class HomeContainer extends Component {
  state = {movies : []}
  searchInput = createRef();

  submitForm = (event) => {
    event.preventDefault();
    const query = this.searchInput.current.value;
    const url = api(query);
    fetch(url)
    .then(response => response.json())
    .then(data => {
      this.setState({data})
    })
    .catch(error => console.error(error))
  }
  render() {
    var data = this.state.data;
    console.log(data);
    return <Home searchInput={this.searchInput} 
    data={this.state.data}
    submitValue={this.submitForm}/>;

  }
}

export default HomeContainer;