import React from 'react';
import PropTypes from 'prop-types'; 
import { Link } from 'react-router-dom';


const Home = ({searchInput, submitValue, data}) => (
  <div>
    <form onSubmit={submitValue}>
    <input ref={searchInput}  required placeholder="Rentrer le nom d'un film"/>
    <button type="submit" >Rechercher</button>
    </form>
    <div>
      {data 
        ? data.Search.map((data) => <Link to={`/movie/${data.imdbID}`}><img src={`${data.Poster}`} alt=""/><br></br>{data.Title}</Link>) 
        : ""
      }
    </div>
  </div>
);

Home.propTypes = {
  searchInput: PropTypes.object.isRequired,
  submitValue: PropTypes.func.isRequired,
};

export default Home;

